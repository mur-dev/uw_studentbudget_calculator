/*
 * @file uw_studentbudget_calculator.js
 * Main library for the uw_studentbudget_calculator  module
 */

(function ($) {
	Drupal.behaviors.exampleModule = {
		attach: function (context, settings) {
			switch($('input:radio[name=uw_studentbudget_calculator_res_boolean]:checked').val()){
				case "0":
					$("#uw_studentbudget_calculator_residence").show();
					$("#uw_studentbudget_calculator_offcampus").hide();
					uw_studentbudget_calculator.calculate();
					break;
				default:
					$("#uw_studentbudget_calculator_residence").hide();
					$("#uw_studentbudget_calculator_offcampus").show();
					uw_studentbudget_calculator.calculate();
					break;
			}

			if ($("#edit-uw-studentbudget-calculator-res-mealplan-values").val() == "0.00" || $("#edit-uw-studentbudget-calculator-res-mealplan-value").val() == "0.00") {
		      $("#uw_oncampus_groceries").show();
			} else {
			  $("#uw_oncampus_groceries").hide();
              document.getElementById("edit-uw-studentbudget-calculator-oncampusgroceries").value = "0";
			  document.getElementById("uw_studentbudget_calculator_oncampusgroceries_total").innerHTML = "$0";
			}
			uw_studentbudget_calculator.calculate();

			// On program change or domestic/international change, update tuition
			$("#edit-uw-studentbudget-calculator-program, input:radio[name=location]").change( function() {
				var location = $("input:radio[name=location]:checked").val();
				$.ajax({
					dataType: "json",
					url: window.location.protocol + "//" + window.location.hostname + "/undergraduate-admissions/financing/budget-calculator/query/program/" + location + "/" + $("#edit-uw-studentbudget-calculator-program").val(),
					success: function(result){
						$("#edit-uw-studentbudget-calculator-tuition").val(result[0].tuition);
						$("#edit-uw-studentbudget-calculator-books").val(result[0].book_costs);
            $("#edit-uw-studentbudget-calculator-fees").val(result[0].fees);
						uw_studentbudget_calculator.calculate();
					}
				});
			});

			// On residence change, update cost and meal plan options
			$("#edit-uw-studentbudget-calculator-res-name").change( function() {
				$.ajax({
					dataType: "json",
					url: window.location.protocol + "//" + window.location.hostname + "/undergraduate-admissions/financing/budget-calculator/query/residence/" + $("#edit-uw-studentbudget-calculator-res-name  option:selected").text(),
					success: function(result){
						var $el = $("#edit-uw-studentbudget-calculator-res-mealplan-names");
						$el.empty(); // remove old options
						for(var i = 0; i < result.length; i++){
						  $el.append($("<option></option>")
						  .attr("value", result[i].price).text(result[i].name));
						  $("#edit-uw-studentbudget-calculator-res-mealplan-value").val($("#edit-uw-studentbudget-calculator-res-mealplan-names").val());

						  if ($("#edit-uw-studentbudget-calculator-res-mealplan-values").val() == "0.00" || $("#edit-uw-studentbudget-calculator-res-mealplan-value").val() == "0.00") {
				            $("#uw_oncampus_groceries").show();
				          } else {
				            $("#uw_oncampus_groceries").hide();
                            document.getElementById("edit-uw-studentbudget-calculator-oncampusgroceries").value = "0";
				            document.getElementById("uw_studentbudget_calculator_oncampusgroceries_total").innerHTML = "$0.00";
				          }
						}

						if ($("#edit-uw-studentbudget-calculator-res-mealplan-values").val() == "0.00" || $("#edit-uw-studentbudget-calculator-res-mealplan-value").val() == "0.00") {
				          $("#uw_oncampus_groceries").show();
				        } else {
				          $("#uw_oncampus_groceries").hide();
                          document.getElementById("edit-uw-studentbudget-calculator-oncampusgroceries").value = "0";
				          document.getElementById("uw_studentbudget_calculator_oncampusgroceries_total").innerHTML = "$0.00";
				        }
				        uw_studentbudget_calculator.calculate();
					}
				});
				$("#edit-uw-studentbudget-calculator-res-value").val($("#edit-uw-studentbudget-calculator-res-name").val());

                if ($("#edit-uw-studentbudget-calculator-res-mealplan-values").val() == "0.00" || $("#edit-uw-studentbudget-calculator-res-mealplan-value").val() == "0.00") {
				  $("#uw_oncampus_groceries").show();
				} else {
				  $("#uw_oncampus_groceries").hide();
                  document.getElementById("edit-uw-studentbudget-calculator-oncampusgroceries").value = "0";
				  document.getElementById("uw_studentbudget_calculator_oncampusgroceries_total").innerHTML = "$0.00";
			    }
				uw_studentbudget_calculator.calculate();
			});

			$("#edit-uw-studentbudget-calculator-res-mealplan-names").change( function() {
				$("#edit-uw-studentbudget-calculator-res-mealplan-value").val($("#edit-uw-studentbudget-calculator-res-mealplan-names").val());

				if ($("#edit-uw-studentbudget-calculator-res-mealplan-values").val() == "0.00" || $("#edit-uw-studentbudget-calculator-res-mealplan-value").val() == "0.00") {
				  $("#uw_oncampus_groceries").show();
				} else {
				  $("#uw_oncampus_groceries").hide();
				  document.getElementById("edit-uw-studentbudget-calculator-oncampusgroceries").value = "0";
				  document.getElementById("uw_studentbudget_calculator_oncampusgroceries_total").innerHTML = "$0.00";
				}
				uw_studentbudget_calculator.calculate();

			});
			$("#edit-uw-studentbudget-calculator-presidents").change( function() {
				$("#edit-uw-studentbudget-calculator-presidents-value").val($("#edit-uw-studentbudget-calculator-presidents").val());
				uw_studentbudget_calculator.calculate();
			});
			$("input:radio[name=uw_studentbudget_calculator_res_boolean]").click(function (){
				switch($(this).val()){
					case "0":
						$("#uw_studentbudget_calculator_residence").show();
						$("#uw_studentbudget_calculator_offcampus").hide();
						uw_studentbudget_calculator.calculate();
						break;
					default:
						$("#uw_studentbudget_calculator_residence").hide();
						$("#uw_studentbudget_calculator_offcampus").show();
						uw_studentbudget_calculator.calculate();
						break;
				}
			});
			$(".uw_studentbudget_calculator_monthly").change(function (){
			if ($(this).val() == '') {
			    $("#uw_studentbudget_calculator_" + $(this).attr("pair") + "_total").text("$0.00");
			}
			else {
                $("#uw_studentbudget_calculator_" + $(this).attr("pair") + "_total").text("$" + (parseFloat(($(this).val()).replace(/,/g,'')) * 8).toFixed(2));
            }
			});
			$("form").bind("reset", function(e) {
				window.location = "interactive-budget";
			});
			uw_studentbudget_calculator.calculate();

		}
	};
})(jQuery);
/**
 * Empty static class wrapper
 */
function uw_studentbudget_calculator() {

};

/**
 * Selects the text in a input field specified by parameter id
 *
 * @param id              ID of the input field to select and put focus on
 */
uw_studentbudget_calculator.selectContents = function(id) {
  try {
	  //alert(id);
      //document.getElementById(id).focus();
      //document.getElementById(id).select();
  }
  catch(e) {
     // Doesn't matter
     alert(e); // debug
  }
}

/**
 * Returns an integer representation of the value read from a input field
 * Returns 0 if no valid value can be read
 */
uw_studentbudget_calculator.getFloatValue = function(id) {
	  try {
	    var element = document.getElementById(id);
	    var value = parseFloat(element.value.replace(/,/g,''));
	    if(isNaN(value)) {
	      //element.value = 0;
	      return 0;
	    }
	    else {
	      var returnItem = value.toFixed(2);
	      if(returnItem.length < 9) {
	        element.value = returnItem;
	        return value;
	      } else {
	    	element.value = 0;
	        return 0.00;
	      }
	    }
	  }
	  catch(e) {
	    return 0;
	  }
	}

/**
 * Calculates the two values (amount * month) and sets the totalId holder with that information
 *
 * @return      Calculated value
 */
uw_studentbudget_calculator.setCalculatedValue = function(totalId, amountId, monthId) {
  var calculatedValue = uw_studentbudget_calculator.getFloatValue(amountId) * uw_studentbudget_calculator.getFloatValue(monthId);
  document.getElementById(totalId).innerHTML = '$' + calculatedValue.toFixed(2);
  return calculatedValue;
}

/**
 * Calculate the value from the forms on the fly
 */
uw_studentbudget_calculator.calculate = function() {
  try {
    // Resources total
    var totalResources = (uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-beginbalance")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-presidents-value")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-awards")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-government-welfare")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-osap-canada")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-otherincome-actions-othervalue")
                          );
    document.getElementById("uw_studentbudget_calculator_monthlyresources_calculated_value").innerHTML = "$" + totalResources.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    // Expenses total
    var totalExpenses = (
                          uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-tuition")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-books")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-fees")
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-personal") * 8
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-transportation") * 8
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-other-expenses") * 8
                          + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-oncampusgroceries") * 8
    );

	var livingOnCampusExpenses = (
		uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-res-value")
		+ uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-res-mealplan-value")
	);
	var livingOffCampusExpenses = (
		uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-offcampusrent")
        + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-offcampusutilities")
        + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-offcampusinternet")
        + uw_studentbudget_calculator.getFloatValue("edit-uw-studentbudget-calculator-offcampusgroceries")
	);

	if (document.getElementById('edit-uw-studentbudget-calculator-res-boolean-0').checked) {
		totalExpenses += livingOnCampusExpenses;
	}
	else {
		totalExpenses += livingOffCampusExpenses * 8;
	}
    document.getElementById("uw_studentbudget_calculator_monthlyexpenses_calculated_value").innerHTML = "$" + totalExpenses.toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    // Financial need
    document.getElementById("uw_studentbudget_calculator_netresources_calculated_value").innerHTML = "$" + (totalResources - totalExpenses).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
  catch(e) {
    // error
    // ignore
  }
}
